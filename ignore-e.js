const URLs = [
  /https?:\/\/github.com(.*)/,
  /https?:\/\/confluence(.*)/,
];
if (URLs.some(regex => window.location.href.match(regex) != null)) {
  ['keydown', 'keyup', 'keypress'].forEach(ev => {
    document.addEventListener(ev, e => {
      if (e.key === "e" && !e.ctrlKey && !e.altKey) {
        e.stopPropagation();
        return false;
      }

      return true;
    }, true);
  });
}
