# ignore-e

Very simple extension for `firefox` because I regularly hit the `e`-key by accident which causes
the following problems:

* on GitHub an editor will be opened and a fork will be created.
* on Confluence an editor will be opened as well.

Since I really hate this behavior, I wrote an extension which discards key-events with `e` as keycode.

Also available under [https://addons.mozilla.org/en-US/firefox/addon/ignore-e/?utm_source=addons.mozilla.org&utm_medium=referral&utm_content=search](https://addons.mozilla.org/en-US/firefox/addon/ignore-e/?utm_source=addons.mozilla.org&utm_medium=referral&utm_content=search).

## Development

* Go to [about:debugging#/runtime/this-firefox](about:debugging#/runtime/this-firefox)
* Press `Load temporary addon` and select `manifest.json` from this repository
* After each change, hit `Reload`
* Build new ZIP with `zip -r -FS ./ignore-e.zip * --exclude '*.git*' --exclude 'test.html'`
